# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))

# -- Project information -----------------------------------------------------

project = "Ket Quantum Programming"
copyright = "2021, Evandro Chagas Ribeiro da Rosa"
author = "Evandro Chagas Ribeiro da Rosa"

# The full version, including alpha/beta/rc tags
# release = 'beta'


# -- General configuration ---------------------------------------------------

master_doc = "index"

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.autosummary",
    "sphinx.ext.napoleon",
    "sphinx.ext.mathjax",
    "sphinx_copybutton",
    "sphinx_inline_tabs",
    "sphinx.ext.intersphinx",
    "sphinx.ext.autosectionlabel",
    "sphinx_design",
]

autosummary_generate = True  # Turn on sphinx.ext.autosummary

autodoc_member_order = "bysource"

intersphinx_mapping = {
    "python": ("https://docs.python.org/3", None),
    "plotly": ("https://plotly.com/python-api-reference", None),
    "ipython": ("https://ipython.org", None),
    "qiskit": ("https://docs.quantum.ibm.com/api/qiskit", None),
}

napoleon_numpy_docstring = True
napoleon_use_param = True

add_module_names = False
add_function_parentheses = False

autodoc_typehints_format = "short"
autodoc_typehints = "signature"

pygments_dark_style = "monokai"

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = "furo"
html_logo = "../logos/ket.svg"
html_title = "Ket Quantum Programming"
html_favicon = "../logos/favicon.ico"
html_theme_options = {
    "sidebar_hide_name": True,
}

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = []
