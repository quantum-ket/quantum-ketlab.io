{% if 'ket' == fullname %}
Ket API
=======
{% else %}
{{ fullname | underline }}
{% endif %}

.. automodule:: {{ fullname }}

{% if modules %}
Modules
-------

.. autosummary::
    :toctree: api

    {% for items in modules %}
        {% if 'clib' != items %}
        {{ items }}
        {% endif %}
    {% endfor %}
{% endif %}

{% if classes %}
{{ ('Classes ``' + fullname + '``') | underline(line='-') }}

.. autosummary::
    :nosignatures:

    {% for items in classes %}
        {{ items }}
    {% endfor %}


    {% for items in classes %}

.. autoclass:: {{ fullname }}.{{ items }}
    :members: 
    {% endfor %}

{% endif %}

{% if functions %}
{{ ('Functions ``' + fullname + '``') | underline(line='-') }}


.. autosummary::

    {% for items in functions %}
        {{ items }}
    {% endfor %}

    {% for items in functions %}
.. autofunction:: {{ fullname }}.{{ items }}
    {% endfor %}

{% endif %}