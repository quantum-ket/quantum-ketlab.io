##########################
Ket Quantum Programming
##########################

Ket is a quantum programming platform that enables the development and execution of quantum-accelerated software. The platform consists of three main components:

* **Ket**: Brings the simplicity of Python programming to quantum computing by providing only the necessary types, functions, and constructs for efficient quantum development. (`Ket repository <https://gitlab.com/quantum-ket/ket>`_)
* **Libket**: The runtime library for the platform, used by Ket in the background, but also accessible directly through C or Rust APIs. (`Libket repository <https://gitlab.com/quantum-ket/libket>`_)
* **Ket Bitwise Simulator** (KBW): A noise-free quantum computer simulator that allows testing of quantum applications on classical computers. KBW supports two simulation methods: Dense simulation (state vector simulation) and Sparse simulation based on the `Bitwise representation <https://arxiv.org/abs/2004.03560>`_. (`KBW repository <https://gitlab.com/quantum-ket/kbw>`_)

Getting Started with Ket
========================

Installation
~~~~~~~~~~~~

Ket requires `Python 3.9 or newer <https://www.python.org/downloads>`_ and is supported on Linux, Windows, and macOS (both Apple silicon and Intel). 
If you are using a non-x86_64 (Intel/AMD) CPU, such as ARM, on Linux or Windows, you will need to install `Rust <https://www.rust-lang.org/tools/install>`_ before proceeding with the Ket installation.

Ket is available on the `Python Package Index (PyPI) <https://pypi.org/project/ket-lang>`_ and can be installed using `pip <https://pip.pypa.io/en/stable/user_guide/>`_. To install Ket, simply copy and paste the following command into your terminal:


.. code-block:: bash

   python3 -m pip install ket-lang


Usage
~~~~~~

You can execute a quantum application using Python interpreter. For example:


.. code-block:: py

    # bell.py
    from ket import *
    p = Process()
    a, b = p.alloc(2)
    CNOT(H(a), b)
    print(dump(a+b).show())

To run the code above, use the command ``python3 bell.py``.

.. code-block:: console

    $ python3 bell.py
    |00⟩    (50.00%)
        0.707107               ≅      1/√2
    |11⟩    (50.00%)
        0.707107               ≅      1/√2


Example: Quantum Teleportation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

With the quantum teleportation protocol, quantum information can be transfered  from one qubit to another using a pair of entangled qubits and quantum measurements.

.. code-block:: py

    # teleport.py
    import ket

    def entangle(a: Quant, b: Quant):
        return CNOT(H(a), b)

    def teleport(quantum_message: Quant, entangled_qubit: Quant):
        adj(entangle)(quantum_message, entangled_qubit)
        return measure(entangled_qubit).value, measure(quantum_message).value

    def decode(classical_message: tuple[int, int], qubit: Quant):
        if classical_message[0] == 1:
            X(qubit)

        if classical_message[1] == 1:
            Z(qubit)

    if __name__ == "__main__":
        p = ket.Process(execution="live")

        alice_message = p.alloc()  # alice_message = |0⟩
        ket.H(alice_message)       # alice_message = |+⟩
        ket.Z(alice_message)       # alice_message = |–⟩

        alice_qubit, bob_qubit = entangle(p.alloc(2))

        classical_message = teleport(
            quantum_message=alice_message,
            entangled_qubit=alice_qubit
        )

        decode(classical_message, bob_qubit)

        ket.H(bob_qubit)  # bob_qubit = |1⟩ 
        bob_m = ket.measure(bob_qubit)
        print("Expected measure 1, result =", bob_m.value)


.. code-block:: console

    $ python teleport.py
    Expected measure 1, result = 1


Used by
=======

.. card:: 
   :link: https://doi.org/10.1109/CEC45853.2021.9504701

   Otto M. Pires, Rafael de Santiago and Jerusa Marchi, "**Two Stage Quantum Optimization for the School Timetabling Problem**", 2021 IEEE Congress on Evolutionary Computation (CEC), 2021, pp. 2347-2353, doi: 10.1109/CEC45853.2021.9504701.

.. card::
   :link: https://qubox.ufsc.br

   Portable GPU-Accelerated Quantum Computer Simulator **QuBOX** at Universidade Federal de Santa Catarina (Brazil).


Cite Ket
========

When using Ket for research projects, please cite:

.. tab:: ACM Ref

    Evandro Chagas Ribeiro da Rosa and Rafael de Santiago. 2021. Ket Quantum Programming. J. Emerg. Technol. Comput. Syst. 18, 1, Article 12 (January 2022), 25 pages. https://doi.org/10.1145/3474224

.. tab:: BibTeX

    .. code-block:: bib 

        @article{ket2021,
            author = {Evandro Chagas Ribeiro da Rosa and Rafael de Santiago},
            title = {Ket Quantum Programming},
            year = {2021},
            issue_date = {January 2022},
            publisher = {Association for Computing Machinery},
            address = {New York, NY, USA},
            volume = {18},
            number = {1},
            issn = {1550-4832},
            url = {https://doi.org/10.1145/3474224},
            doi = {10.1145/3474224},
            journal = {J. Emerg. Technol. Comput. Syst.},
            month = {oct},
            articleno = {12},
            numpages = {25},
            keywords = {Quantum programming, cloud quantum computation, qubit simulation}
        }


Download the original article that proposes the Ket language: :download:`pdf <_static/ket_article.pdf>`

Ket API Documentation
=====================

.. autosummary::
    :toctree: ket
    :recursive:

    ket

.. toctree::
    :maxdepth: 2
    :hidden:

    guide
    runtime
    ibm
    Source Code <https://gitlab.com/quantum-ket>

----------------

.. image:: _static/sponsor.svg
    :width: 66%
    :class: only-light, no-scaled-link
    :alt: Sponsored by Quantuloop
    :target: https://quantuloop.com

.. image:: _static/sponsor_dark.svg
    :width: 66%
    :class: only-dark, no-scaled-link
    :alt: Sponsored by Quantuloop
    :target: https://quantuloop.com

----------------

.. admonition:: Contact
    :class: seealso

    ketteam@quantuloop.com
    