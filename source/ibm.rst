####################################
Running Ket on IBM Quantum Computers
####################################

Ket allows you to execute quantum applications on IBM’s quantum computers, making it possible to move from simulation to real quantum hardware. To use IBM's quantum resources, you need to configure the quantum process’s execution target accordingly.

Execution Capabilities
======================

Each simulator and quantum computer offers unique capabilities, which should be taken into account when selecting the execution target’s configuration. When deploying your Ket application on an IBM quantum computer, the process is configured to run in "batch" mode. This batch execution differs from simulations by disabling certain features available during the simulation stage, ensuring compatibility with hardware constraints.

  **Login into IBM and get your Token**: To access IBM’s quantum computers, you need to create an account on the `IBM Quantum <https://quantum.ibm.com/>`_ platform. After creating an account, you will receive an API token that allows you to access IBM’s quantum computers.

  .. image:: _static/ibm_token.png
      :width: 50%
      :class: no-scaled-link
      :alt: Where to get IBM access Token
      :align: center

  **Select an IBM Quantum Device**: To set up Ket for an IBM quantum computer, you will need to specify the target device. Devices vary by qubit count, connectivity, gate fidelity, and queue times, so it’s essential to choose a device that best suits the requirements of your application. Alternatively, you can use the default device, which is the least busy quantum computer available.

  .. image:: _static/ibm_targets.png
      :width: 90%
      :class: no-scaled-link
      :alt: IBM target computers
      :align: center

  And the following code:

  .. code-block:: py

    from ket.ibm import IBMDevice
    from qiskit_ibm_runtime import QiskitRuntimeService

    service = QiskitRuntimeService(
        channel='ibm_quantum',
        instance='ibm-q/open/main',
        token='YOUR_TOKEN'
    )

    # Will automatically use the least busy backend
    backend = service.least_busy()

    # Will use the specified backend
    # backend = service.backend('ibm_brisbane')

    # Create an instance of the IBM quantum device with the choosen backend
    device = IBMDevice(backend)

  **Execute the Process**: Once the target device is configured, Ket handles the submission of your quantum circuit as a job to IBM’s cloud. Example:

  .. code-block:: py

    process = Process(device.configure())
    a, b = process.alloc(2)
    CNOT(H(a), b)

    sample(a + b).histogram()

  **Wait for jobs**: After submitting your quantum circuit, you will receive a job ID that you can use to monitor the job’s progress. Beware that the execution time may vary depending on the device’s queue.

  .. image:: _static/ibm_jobs.png
      :width: 90%
      :class: no-scaled-link
      :alt: Where to get IBM access Token
      :align: center

  **Handle Device Constraints**: Also keep in mind that real quantum devices are subject to noise and limited gate operations. Ket manages these constraints by optimizing the circuit for the specific hardware whenever possible.

By following these steps, you can deploy your Ket quantum processes on IBM’s quantum computers, expanding the practical applications of your projects beyond simulation.